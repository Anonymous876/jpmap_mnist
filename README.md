## Run JPMAP algorithm

1. Install Miniconda and create new environment containing required packages with the following commands:

2. - **conda create -n jpmap python=3.7 matplotlib imageio scipy**
   - **conda activate jpmap**
   - **conda install -c anaconda tensorflow-gpu=1.14**

3. Ejecute main script: **bash run_experiments.sh** (saves results in folder **experiments/mnist/VAEfc/results**). It contains example experiments of Inpainting and Compressed Sensing as shown in the submitted paper.

