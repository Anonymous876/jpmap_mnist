import numpy as np
from numpy.linalg import norm as np_norm
from numpy.linalg import solve, lstsq, inv
from scipy.sparse.linalg import cg as scipy_cg

import tensorflow as tf
import tensorflow.contrib.distributions as tfd

from utils import *


def z_step(sess, xk, zold, vae, max_iters=1000, verbose=False, k=0):
    
    lr_adam = 0.005  # MNIST

    Enc = vae['encoder']
    Dec = vae['decoder']
    model = vae['model']

    if k<=10:
        z1 = Enc(xk[None,:])[0][0]  # Init with mu_E(x^k)
    else:
        z1 = zold

    # D(x^k, z)
    Dxz = 0.5*tf.reduce_sum(tf.square(xk[None,:] - model.x_hat) / model.gamma_x)

    # lossF = 0.5*||z||^2 + D(x^k, z)
    lossF = 0.5*tf.reduce_sum(tf.square(model.z)) + Dxz
    
    gradF = tf.gradients(lossF, [model.z])
    
    z2 = adam_tf(sess, z1, model, gradF, step_size=lr_adam, max_iters=max_iters)
    mu_D, gamma_D = Dec(z2[None,:])  # For next x step

    output = (z2, [mu_D[0], gamma_D[0]])

    return output


def x_step_A(xtilde, x_shape, sigma, A, decoder_outputs):
    
    mu_D, Sigma_D_diag = decoder_outputs['outputs']

    mu_D = mu_D.reshape(-1)
    Sigma_D_diag = Sigma_D_diag.reshape(-1)
    Amatrix = np.dot(A.T,A) + ((sigma**2)/Sigma_D_diag)*np.eye(mu_D.size)

    x_new = solve(Amatrix, np.dot(A.T,xtilde) + ((sigma**2)/Sigma_D_diag)*mu_D)
    x_new = x_new.reshape(x_shape).astype(xtilde.dtype)
    
    return x_new


def precompute_x_step_inv(xtilde, x_shape, sigma, A, gamma):
    # INPUT: xtilde: noisy input
    #        sigma: noise std
    #        A: forward (linear) operator
    #        gamma: parameter of decoder covariance gamma*I
    #
    # OUTPUT:
    #        Minv:  inv(A^T*A + (sigma^2/gamma)I)
    
    M = np.dot(A.T,A) + ((sigma**2)/gamma)*np.eye(A.shape[1])
    
    return inv(M)


def x_step_A_precomputed(xtilde, x_shape, sigma, A, Minv, decoder_outputs):
    # INPUT: xtilde: noisy input
    #        sigma: noise std
    #        A: forward (linear) operator
    #
    # OUTPUT:
    #        x step:  argmin_x (1/2*sigma^2)*||Ax-xtilde||^2 + D(x, z^k)
    
    mu_D, gamma = decoder_outputs['outputs']
    mu_D = mu_D.reshape(-1)

    x_new = np.dot(Minv, np.dot(A.T,xtilde) + ((sigma**2)/gamma)*mu_D)
    x_new = x_new.reshape(x_shape).astype(xtilde.dtype)
    
    return x_new


def restore_framework(sess, xtilde, x_shape, vae, A, sigma, z_dim, xtarget=None, tol=1e-5, max_iters=200, verbose=True, zinit=None, xinit=None):

    # Setup
    Dec = vae['decoder']
    
    n = xtilde.size  # Number of pixels
    terminate = 0  # Main loop flag
    k = 0  # Iteration counter
    
    if zinit is None:
        z = np.zeros(z_dim)
    else:
        z = zinit.copy()

    # A full matrix:
    if xinit is None:
        xn = np.dot(A.T,xtilde).reshape(x_shape)
    else:
        xn = xinit.copy()
    
    decoder_outputs = {}
    
    # Adam parameters (for z step)
    max_iters_z = 2000
    
    gamma = Dec(z[None,:])[1]
    
    print('Precomputing matrix for x-step...')
    Minv = precompute_x_step_inv(xtilde, x_shape, sigma, A, gamma)

    # Main loop
    while not terminate:
        
        ### z step (regularization)
        output = z_step(sess, xn, z, vae, max_iters_z, k=k)
        [zn, dec_outs] = output[0], output[1]
        
        ### x step (data fit)
        decoder_outputs['outputs'] = dec_outs
        xn = x_step_A_precomputed(xtilde, x_shape, sigma, A, Minv, decoder_outputs)
        
        ### Convergence criterion
        delta = (np_norm(x-xn) + np_norm(z-zn)) / np.sqrt(n) if k!=0 else np.Inf

        if verbose:
            print('ITERATION %d -->  '%(k+1), 'Delta: %.5f  |  MSE to ground-truth: %.5f' % (delta, compute_mse(xtarget, xn)))

        # Update parameters
        k += 1
        x = xn
        z = zn

        if (k>=max_iters) or (delta < tol):
            terminate = 1

    if verbose:
        print('Restoration terminated in %d iterations (out of %d)' % (k, max_iters))
    
    return [x,z]


# Custom CSGM implementation (Bora et al.)
def CSGM_bora(sess, xtilde, zdim, A, vae, lamb=0.1, max_iters=2000, xtarget=None, zinit=None):
    # Bora, A., Jalal, A., Price, E., & Dimakis, A. G. (2017, August).
    # Compressed sensing using generative models. In Proceedings of
    # the 34th International Conference on Machine Learning-Volume 70
    # (pp. 537-546). JMLR. org.  https://arxiv.org/abs/1703.03208
    #
    # Computes G(argmin_z ||A.*G(z) - xtilde||^2 + lamb*||z||^2)

    print('Running Bora et al. algorithm...')

    model = vae['model']
    Dec = vae['decoder']

    ### CSGM's loss function:
    lr_adam = 0.01

    if vae['stage']==1:
 
        ## Datafit = ||A*Dec(z) - xtilde||^2
        AdotDecz = tf.linalg.matvec(A, tf.reshape(model.x_hat, [1,-1]))
        Datafit = tf.reduce_sum(tf.square(AdotDecz - xtilde))

        ## Reg = ||z||^2
        Reg = tf.reduce_sum(tf.square(model.z))

        # lossF = ||A*Dec(z) - xtilde||^2 + lambda*||z||^2
        lossF = Datafit + lamb*Reg

    elif vae['stage']==2:
        raise NotImplementedError

    gradF = tf.gradients(lossF, [model.z])

    if zinit is None:
        zinit = np.random.randn(zdim)

    zopt = adam_tf(sess, zinit, model, gradF, step_size=lr_adam, max_iters=max_iters)

    xopt = Dec(zopt[None,:])[0][0]

    if xtarget is not None:
        print(' --> MSE to ground-truth: %.5f' % compute_mse(xtarget, xopt))

    return [xopt, zopt]
