# Inpainting
python run_framework_TwoStageVAE.py --exp-name VAEfc --exp-id 1 --problem missingpixels --sigma 2 --missing 0.85 --n-samples 10

# Compressed Sensing
python run_framework_TwoStageVAE.py --exp-name VAEfc --exp-id 2 --problem compsensing --sigma 2 --output-dim 150 --n-samples 10