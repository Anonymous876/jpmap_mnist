import numpy as np
from numpy.linalg import norm as np_norm
import os, shutil
import tensorflow as tf


def load_model(args, side_length, channels, model_path):
    tf.reset_default_graph()
    sess = tf.InteractiveSession()
    input_x = tf.placeholder(tf.uint8, [args.batch_size, side_length, side_length, channels], 'x')
    model = eval(args.network_structure)(input_x, args.latent_dim, args.second_depth, args.second_dim, args.cross_entropy_loss)
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver()
    saver.restore(sess, os.path.join(model_path, 'stage2'))

    return sess, model

def save_results(path, output_dict_list):
    
    n_iters = len(output_dict_list)

    for i in range(n_iters):
        
        path_iter = path + 'iter' + str(i+1) + '/'

        if os.path.isdir(path_iter):
            shutil.rmtree(path_iter)
        os.makedirs(path_iter)
        output_dict = output_dict_list[i]

        for varname in ['zvar0_iters', 'zvar2_iters', 'Jz0_iters', 'Jz2_iters', 'zs', 'Jarray', 'xn']:
            np.savetxt(path_iter+varname, output_dict[varname])
        
        for varname in ['delta', 'tol', 'num_iters', 'lr_z', 'max_iters_z', 'mse']:
            np.save(path_iter+varname, output_dict[varname])

        print('  --> Variables saved in ' + path_iter + ' !')


def stich_imgs(x, img_per_row=10, img_per_col=10):
    x_shape = np.shape(x)
    assert(len(x_shape) == 4)
    output = np.zeros([img_per_row*x_shape[1], img_per_col*x_shape[2], x_shape[3]])
    idx = 0
    for r in range(img_per_row):
        start_row = r * x_shape[1]
        end_row = start_row + x_shape[1]
        for c in range(img_per_col):
            start_col = c * x_shape[2]
            end_col = start_col + x_shape[2]
            output[start_row:end_row, start_col:end_col] = x[idx]
            idx += 1
            if idx == x_shape[0]:
                break
        if idx == x_shape[0]:
            break
    if np.shape(output)[-1] == 1:
        output = np.reshape(output, np.shape(output)[0:2])

    return output


def adam_tf(sess, zinit, model, gradF, step_size=0.01, tol=1e-5, max_iters=5000, verbose=False, iters=False, lossF=None):
    # Adam of loss function F implemented in TF (with gradient = gradF)
    # zinit: z initialization (WITHOUT fake 1st dimension!)
    # input_tensor: tensor to feed input data in
    
    # Parameters of Adam:
    beta_1 = 0.9
    beta_2 = 0.999
    epsilon = 1e-8
    m = 0
    v = 0
    
    # Initialization:
    k = 0
    terminate = 0
    N = zinit.size  # Number of dimensions
    z = zinit.copy()
    z_iters = [z.copy()]
    if lossF is not None:
        Jz_iters = [sess.run(lossF, feed_dict={input_tensor: z[None,:]})]

    # Main loop:
    while not terminate:
        k += 1
        grz = sess.run(gradF, feed_dict={model.z: z[None,:], model.is_training: False})[0][0]
        
        m = beta_1 * m + (1 - beta_1) * grz
        v = beta_2 * v + (1 - beta_2) * grz**2
        m_hat = m / (1 - beta_1**k)
        v_hat = v / (1 - beta_2**k)

        z -= step_size * m_hat / (np.sqrt(v_hat) + epsilon)
        z_iters.append(z.copy())
        if lossF is not None:
            Jz_iters.append(sess.run(lossF, feed_dict={input_tensor: z[None,:]}))
        
        terminate = (np_norm(grz)/N < tol) or (k >= max_iters)

    if verbose:
        print('Adam terminated in %d iterations (out of %d)' % (k, max_iters))
        print('np_norm(grz)/N at last iteration: %.4f' % (np_norm(grz)/N))
    
    return z


def compute_mse(xtarget, xopt):
    n = xtarget.size  # Number of pixels

    return np_norm(xtarget - xopt)**2 / n
