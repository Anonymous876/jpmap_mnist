import argparse 
import os 
from network.two_stage_vae_model import *
import numpy as np 
import tensorflow as tf 
import math 
import time 
import matplotlib.pyplot as plt 
from imageio import imsave
from dataset import load_dataset, load_test_dataset

from framework_VAE_TwoStage import *

from utils import *


##### Load parameters #####

parser = argparse.ArgumentParser()
parser.add_argument('--root-folder', type=str, default='.')
parser.add_argument('--output-path', type=str, default='./experiments')
parser.add_argument('--exp-name', type=str, default='framework_MNIST')

parser.add_argument('--dataset', type=str, default='mnist')
parser.add_argument('--gpu', type=int, default=0)

parser.add_argument('--network-structure', type=str, default='VAEfc')
parser.add_argument('--batch-size', type=int, default=1)
parser.add_argument('--write-iteration', type=int, default=600)
parser.add_argument('--latent-dim', type=int, default=12)

parser.add_argument('--second-dim', type=int, default=2048)
parser.add_argument('--second-depth', type=int, default=3)

parser.add_argument('--num-scale', type=int, default=3)
parser.add_argument('--block-per-scale', type=int, default=1)
parser.add_argument('--depth-per-block', type=int, default=2)
parser.add_argument('--kernel-size', type=int, default=3)
parser.add_argument('--base-dim', type=int, default=32)
parser.add_argument('--fc-dim', type=int, default=512)

parser.add_argument('--epochs', type=int, default=400)
parser.add_argument('--lr', type=float, default=0.0001)
parser.add_argument('--lr-epochs', type=int, default=150)
parser.add_argument('--lr-fac', type=float, default=0.5)

parser.add_argument('--epochs2', type=int, default=800)
parser.add_argument('--lr2', type=float, default=0.0001)
parser.add_argument('--lr-epochs2', type=int, default=300)
parser.add_argument('--lr-fac2', type=float, default=0.5)
parser.add_argument('--cross-entropy-loss', default=False, action='store_true')

parser.add_argument('--val', default=True, action='store_true')

parser.add_argument('--problem', type=str, default='missing_pixels')  # 'compsensing', 'denoising', 'missingpixels'
parser.add_argument('--output-dim', type=int, default=100)
parser.add_argument('--exp-id', type=int, default=0)
parser.add_argument('--n-samples', type=int, default=10)
parser.add_argument('--sigma', type=int, default=2)
parser.add_argument('--init', type=int, default=50)
parser.add_argument('--missing', type=float, default=0.8)

args = parser.parse_args()

os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu)


##### Main script #####

# experiment info
exp_folder = os.path.join(args.output_path, args.dataset, args.exp_name)
if not os.path.exists(exp_folder):
    os.makedirs(exp_folder)
model_path = os.path.join(exp_folder, 'model')
if not os.path.exists(model_path):
    os.makedirs(model_path)

# test dataset 
dataset, side_length, channels = load_test_dataset(args.dataset, args.root_folder)
dataset = dataset.astype(np.float32) / 255.0

np.random.seed(1)
n_samples = args.n_samples
x_target = dataset[args.init:args.init+n_samples]
x_dims = x_target.shape[1:]  # Image shape
x_size = x_target[0].size  # Image size (total number of pixels)

sigma = args.sigma/255.


if args.problem == 'compsensing':
    output_dim = args.output_dim
    A = (1./np.sqrt(output_dim))*np.random.randn(output_dim, x_size).astype(np.float32)

elif args.problem == 'denoising':
    output_dim = x_size
    A = np.eye(x_size, dtype=np.float32)

elif args.problem == 'missingpixels':
    output_dim = x_size
    missing_pixels = args.missing
    if x_dims[2] == 1:  # 1 channel = Grayscale image
        mask = np.random.rand(output_dim)>missing_pixels
    elif x_dims[2] == 3:  # 3 channels = RGB image
        mask = 1.*(np.random.rand(*x_dims[0:2])>missing_pixels)
        mask = np.dstack(3*[mask]).reshape(-1)
    A = np.diag(mask).astype(np.float32)


x_noisy = np.zeros((n_samples, output_dim))
for i in range(n_samples):
    x_noisy[i,:] = np.dot(A, x_target[i,:,:,:].reshape(-1))
    
x_noisy += sigma*np.random.randn(*x_noisy.shape)

x_results_jpmap = np.zeros(x_target.shape)
z_results_jpmap = np.zeros((x_target.shape[0], args.latent_dim))
x_results_csgm = np.zeros(x_target.shape)
z_results_csgm = np.zeros((x_target.shape[0], args.latent_dim))


for ind in range(n_samples):
    xtilde = x_noisy[ind,:].astype(np.float32)
    
    xtarget = x_target[ind, :]  # Only to compute errors (not mandatory)
    
    tf.reset_default_graph()
    input_x = tf.placeholder(tf.uint8, [args.batch_size, side_length, side_length, channels], 'x')
    
    if args.network_structure == 'Resnet_MNIST':
        model = Resnet_MNIST(input_x, args.num_scale, args.block_per_scale, args.depth_per_block, args.kernel_size, args.base_dim, args.fc_dim, args.latent_dim, args.second_depth, args.second_dim, args.cross_entropy_loss)
    elif args.network_structure == 'Resnet_CelebA':
        model = Resnet_CelebA(input_x, args.num_scale, args.block_per_scale, args.depth_per_block, args.kernel_size, args.base_dim, args.fc_dim, args.latent_dim, args.second_depth, args.second_dim, args.cross_entropy_loss)
    else:
        model = eval(args.network_structure)(input_x, args.latent_dim, args.second_depth, args.second_dim, args.cross_entropy_loss)
    
    sess = tf.InteractiveSession()
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver()
    saver.restore(sess, os.path.join(model_path, 'stage2'))

    stage = 1
    Enc = lambda x: model.encoder(sess, x, stage)
    Dec = lambda z: model.decoder(sess, z, stage)

    vae = {}
    vae['decoder'] = Dec
    vae['encoder'] = Enc
    vae['model']   = model
    vae['stage'] = 1

    # Verbose
    print()
    print('#################################################')
    print('Restoring image %d of %d...' % (ind+1, n_samples))

    # Same initialization for both algorithms
    zinit = np.zeros(args.latent_dim)

    # CSGM (Bora et al.)
    lamb = 0.01  # Strength of regularization
    [xopt_csgm, zopt_csgm] = CSGM_bora(sess, xtilde, args.latent_dim, A, vae, lamb, max_iters=2000, xtarget=xtarget, zinit=zinit)
    x_results_csgm[ind,:] = xopt_csgm
    z_results_csgm[ind,:] = zopt_csgm

    # JPMAP
    [xout_jpmap, zout_jpmap] = restore_framework(sess, xtilde, x_dims, vae, A, sigma, args.latent_dim, xtarget=xtarget, max_iters=200, zinit=zinit)

    x_results_jpmap[ind,:] = xout_jpmap
    z_results_jpmap[ind,:] = zout_jpmap

    sess.close()
    
x_target_block    = stich_imgs(x_target.reshape(-1, x_dims[0], x_dims[1], x_dims[2]), 1, n_samples)
if args.problem != 'compsensing':
    x_noisy_block     = stich_imgs(x_noisy.reshape(-1, x_dims[0], x_dims[1], x_dims[2]), 1, n_samples)
x_csgm_block      = stich_imgs(x_resultados_csgm.reshape(-1, x_dims[0], x_dims[1], x_dims[2]), 1, n_samples)
x_framework_block = stich_imgs(x_resultados_framework.reshape(-1, x_dims[0], x_dims[1], x_dims[2]), 1, n_samples)


tf.reset_default_graph()
sess = tf.InteractiveSession()
input_x = tf.placeholder(tf.uint8, [args.batch_size, side_length, side_length, channels], 'x')

if args.network_structure == 'Resnet_MNIST':
    model = Resnet_MNIST(input_x, args.num_scale, args.block_per_scale, args.depth_per_block, args.kernel_size, args.base_dim, args.fc_dim, args.latent_dim, args.second_depth, args.second_dim, args.cross_entropy_loss)
elif args.network_structure == 'Resnet_CelebA':
    model = Resnet_CelebA(input_x, args.num_scale, args.block_per_scale, args.depth_per_block, args.kernel_size, args.base_dim, args.fc_dim, args.latent_dim, args.second_depth, args.second_dim, args.cross_entropy_loss)
else:
    model = eval(args.network_structure)(input_x, args.latent_dim, args.second_depth, args.second_dim, args.cross_entropy_loss)

sess.run(tf.global_variables_initializer())
saver = tf.train.Saver()
saver.restore(sess, os.path.join(model_path, 'stage2'))


mse_csgm = np.zeros(n_samples)
mse_framework = np.zeros(n_samples)

for i in range(n_samples):
    mse_csgm[i]      = compute_mse(x_target[i,:], x_resultados_csgm[i,:])
    mse_framework[i] = compute_mse(x_target[i,:], x_resultados_framework[i,:])

results_folder = os.path.join(exp_folder,'results','exp%d'%args.exp_id)
if not os.path.exists(results_folder):
    os.makedirs(results_folder)

np.save(os.path.join(results_folder, 'mse_outputdim%d_csgm'%output_dim), mse_csgm)
np.save(os.path.join(results_folder, 'mse_outputdim%d_framework'%output_dim), mse_framework)

x_reconst = model.reconstruct(sess, x_target)
x_reconst_block = stich_imgs(x_reconst, 1, n_samples)

if args.problem != 'compsensing':
    figure = np.concatenate([x_target_block, x_noisy_block, x_csgm_block, x_framework_block, x_reconst_block], axis=0)
else:
    figure = np.concatenate([x_target_block, x_csgm_block, x_framework_block, x_reconst_block], axis=0)

figure = np.clip(figure, 0., 1.)

plt.imsave(os.path.join(results_folder, 'restored_outputdim%d-exp%d.png'%(output_dim,args.exp_id)), figure, cmap='gray')
